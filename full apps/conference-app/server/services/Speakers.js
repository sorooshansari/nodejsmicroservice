const axios = require("axios");
const url = require("url");
const crypto = require("crypto");
const CircuitBreaker = require("../lib/circuit-breaker");
const circuitBreaker = new CircuitBreaker();
const fs = require("fs");
const util = require("util");
const fsExists = util.promisify(fs.exists);

class SpeakersService {
  constructor({ serviceRegistryUrl, serviceVersionIdentifier }) {
    this.serviceRegistryUrl = serviceRegistryUrl;
    this.serviceVersionIdentifier = serviceVersionIdentifier;
    this.cache = {};
  }

  async getImage(path) {
    console.log("imagePath", path);
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      responseType: "stream",
      url: `http://${ip}:${port}/images/${path}`
    });
  }

  async getNames() {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/names`
    });
  }

  async getListShort() {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/list-short`
    });
  }

  async getList() {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/list`
    });
  }

  async getAllArtwork() {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/artwork`
    });
  }

  async getSpeaker(shortname) {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/speaker/${shortname}`
    });
  }

  async getArtworkForSpeaker(shortname) {
    const { ip, port } = await this.getService("speakers-service");
    return await this.callService({
      method: "get",
      url: `http://${ip}:${port}/artwork/${shortname}`
    });
  }

  async callService(requestOptions) {
    const requestPath = url.parse(requestOptions.url).path;
    const cacheKey = crypto
      .createHash("md5")
      .update(requestOptions.method + requestPath)
      .digest("hex");
    let cacheFile = null;
    if (
      requestOptions.responseType &&
      requestOptions.responseType === "stream"
    ) {
      cacheFile = `${__dirname}/../../_imageCache/${cacheKey}`;
    }
    const result = await circuitBreaker.callService(requestOptions);
    if (!result) {
      if (this.cache[cacheKey]) {
        return this.cache[cacheKey];
      }
      if (cacheFile) {
        const exists = await fsExists(cacheFile);
        if (exists) return fs.createReadStream(cacheFile);
      }
      return false;
    }
    if (!cacheFile) {
      this.cache[cacheKey] = result;
    } else {
      const ws = fs.createWriteStream(cacheFile);
      result.pipe(ws);
    }
    return result;
  }

  async getService(serviceName) {
    const response = await axios.get(
      `${this.serviceRegistryUrl}/find/${serviceName}/${this.serviceVersionIdentifier}`
    );
    return response.data;
  }
}

module.exports = SpeakersService;
