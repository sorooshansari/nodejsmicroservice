const semver = require("semver");

class ServiceRegistry {
  constructor(log) {
    this.log = log;
    this.services = {};
    this.timeout = 30;
  }

  get(name, version) {
    this.cleanup();
    const candidates = Object.values(this.services).filter(
      service =>
        service.name === name && semver.satisfies(service.version, version)
    );
    return candidates[Math.floor(Math.random() * candidates.length)];
  }
  register(name, version, ip, port) {
    this.cleanup();
    const key = name + version + ip + port;
    if (!this.services[key]) {
      this.services[key] = {
        name,
        version,
        ip,
        port,
        timeStamp: Math.floor(Date.now() / 1000)
      };
      this.log.debug(`Service added: ${name}, ${version}, ${ip}:${port}`);
    } else {
      this.services[key].timeStamp = Math.floor(Date.now() / 1000);
      this.log.debug(`Service updated: ${name}, ${version}, ${ip}:${port}`);
    }
    return key;
  }

  unregister(name, version, ip, port) {
    const key = name + version + ip + port;
    delete this.services[key];
    this.log.debug(`Service unregistered: ${name}, ${version}, ${ip}:${port}`);
    return key;
  }

  cleanup() {
    const now = Math.floor(Date.now() / 1000);
    Object.keys(this.services).forEach(key => {
      if (this.services[key].timeStamp + this.timeout < now) {
        delete this.services[key];
        console.debug(`Removed service ${key}`);
      }
    });
  }
}

module.exports = ServiceRegistry;
